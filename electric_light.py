class Light:
    def __init__(self):
        self.front = False
        self.back = False

    def toggle(self):
        if self.front:
            self.front = False
        else:
            self.front = True

        if self.back:
            self.back = False
        else:
            self.back = True
