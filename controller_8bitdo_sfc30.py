from evdev import ecodes

last = {
    "ABS_Y": 127,
    "ABS_X": 127
}

btnA = 304
btnB = 305
btnY = 308
btnX = 307
btnL = 310
btnR = 311
btnStart = 315
btnSelect = 314


class Controller8bitdoSfc30:
    def __init__(self, controller, gamepad):
        self.Controller = controller
        self.Gamepad = gamepad
        self.loop()

    def loop(self):
        # evdev takes care of polling the controller in a loop
        for event in self.Gamepad.read_loop():
            if event.type == ecodes.EV_KEY:
                if event.value == 1:
                    if event.code == btnA:
                        self.Controller.honk()
                    if event.code == btnB:
                        print('')
                    if event.code == btnX:
                        print('')
                    if event.code == btnY:
                        self.Controller.toggleLight()
                    if event.code == btnL:
                        self.Controller.reverse(100)
                    if event.code == btnR:
                        self.Controller.forward(100)
                    if event.code == btnStart:
                        print('start')
                    if event.code == btnSelect:
                        self.Controller.toggleControl()
                if event.value == 0:
                    if event.code == btnL:
                        self.Controller.stop()
                    if event.code == btnR:
                        self.Controller.stop()

            if event.type == ecodes.EV_ABS:
                if event.type == ecodes.EV_ABS:
                    if event.code == ecodes.ABS_Y:
                        last["ABS_Y"] = event.value

                    if event.code == ecodes.ABS_X:
                        last["ABS_X"] = event.value

                    """if last["ABS_Y"] > 127:
                        self.Controller.forward(100)
                    elif last["ABS_Y"] < 127:
                        self.Controller.reverse(100)"""

                    if last["ABS_X"] > 127:
                        self.Controller.right()
                    elif last["ABS_X"] < 127:
                        self.Controller.left()

                    """if last["ABS_Y"] == 127:
                        self.Controller.stop()"""
                    if last["ABS_X"] == 127:
                        self.Controller.center()
