import RPi.GPIO as GPIO
import time


class Direction:
    def __init__(self):
        self.pin1 = 18
        self.pin2 = 16
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.pin1, GPIO.OUT)  # set GPIO 05 as an output.
        GPIO.setup(self.pin2, GPIO.OUT)  # set GPIO 04 as an output.

        self.p1 = GPIO.PWM(self.pin1, 100)
        self.p2 = GPIO.PWM(self.pin2, 100)

    def right(self):
        self.p1.start(100)
        self.p2.start(0)

    def left(self):
        self.p1.start(0)
        self.p2.start(100)

    def stop(self):
        self.p1.start(0)
        self.p2.start(0)