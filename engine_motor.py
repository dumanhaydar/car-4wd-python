import RPi.GPIO as GPIO
import time


class Motor:
    def __init__(self, engine):
        self.engine = engine
        self.pin1 = 13  # Speed
        self.pin2 = 15
        self.pin3 = 11
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.pin1, GPIO.OUT)  # set GPIO 05 as an output.
        GPIO.setup(self.pin2, GPIO.OUT)  # set GPIO 04 as an output.
        GPIO.setup(self.pin3, GPIO.OUT)  # set GPIO 04 as an output.

        self.f1 = GPIO.PWM(self.pin1, 100)  # Speed
        self.f2 = GPIO.PWM(self.pin2, 100)
        self.f3 = GPIO.PWM(self.pin3, 100)

        self.pin4 = 40  # Speed
        self.pin5 = 38
        self.pin6 = 36
        GPIO.setup(self.pin4, GPIO.OUT)  # set GPIO 05 as an output.
        GPIO.setup(self.pin5, GPIO.OUT)  # set GPIO 04 as an output.
        GPIO.setup(self.pin6, GPIO.OUT)  # set GPIO 04 as an output.

        self.b1 = GPIO.PWM(self.pin4, 100)  # Speed
        self.b2 = GPIO.PWM(self.pin5, 100)
        self.b3 = GPIO.PWM(self.pin6, 100)

    def drive(self, speed):
        if (self.engine.xdrive == '4WD') or (self.engine.xdrive == 'FWD'):
            self.f1.start(speed)
            self.f2.start(0)
            self.f3.start(100)

        if (self.engine.xdrive == '4WD') or (self.engine.xdrive == 'BWD'):
            self.b1.start(speed)
            self.b2.start(0)
            self.b3.start(100)

    def reverse(self, speed):
        speed = int(round(speed / 4 * 3))
        if (self.engine.xdrive == '4WD') or (self.engine.xdrive == 'FWD'):
            self.f1.start(speed)
            self.f2.start(100)
            self.f3.start(0)

        if (self.engine.xdrive == '4WD') or (self.engine.xdrive == 'BWD'):
            self.b1.start(speed)
            self.b2.start(100)
            self.b3.start(0)

    def stop(self):
        time.sleep(0.3)
        self.f1.start(0)
        self.f2.start(0)
        self.f3.start(0)
        self.b1.start(0)
        self.b2.start(0)
        self.b3.start(0)
