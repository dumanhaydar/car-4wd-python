from controller_8bitdo_sfc30 import Controller8bitdoSfc30
from evdev import InputDevice

gamepad = InputDevice('/dev/input/event0')
print(gamepad)


class ElectricController:
    def __init__(self, engine, motor, direction, light, honk):
        self.press = False
        self.Engine = engine
        self.Motor = motor
        self.Direction = direction
        self.Light = light
        self.Honk = honk
        self.start()

    def start(self):
        if gamepad.name.find('SFC30') != -1:
            Controller8bitdoSfc30(self, gamepad)

    def toggleLight(self):
        self.Light.toggle()

    def honk(self):
        self.Honk.press()

    def forward(self, speed):
        self.Motor.drive(speed)

    def reverse(self, speed):
        self.Motor.reverse(speed)

    def stop(self):
        self.Motor.stop()

    def right(self):
        self.Direction.right()

    def left(self):
        self.Direction.left()

    def center(self):
        self.Direction.stop()

    def toggleControl(self):
        self.Engine.toggleControl()

    def toggleXdrive(self):
        self.Engine.toggleXdrive()
