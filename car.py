from electric_controller import ElectricController
from engine_motor import Motor
from engine_direction import Direction
from electric_transmission import Transmission
from electric_light import Light
from electric_honk import Honk
from engine import Engine

engine = Engine()
motor = Motor(engine)
direction = Direction()
transmission = Transmission()
light = Light()
honk = Honk()

controller = ElectricController(engine, motor, direction, light, honk)


#  Manual(Engine, Motor, Transmission, Light, Honk)
