class Engine:
    def __init__(self):
        """ Control: controller, manual """
        self.control = 'controller'
        """Wheel: FWD,4WD"""
        self.xdrive = '4WD'

    def toggleControl(self):
        if self.control == 'controller':
            self.control = 'manual'
        if self.control == 'manual':
            self.control = 'controller'

    def toggleXdrive(self):
        if self.xdrive == '4WD':
            self.xdrive = 'FWD'
        if self.xdrive == 'FWD':
            self.xdrive = 'BWD'
        if self.xdrive == 'BWD':
            self.xdrive = '4WD'
