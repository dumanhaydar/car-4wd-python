class Honk:
    def __init__(self):
        self.pressed = False

    def press(self):
        self.pressed = True

    def release(self):
        self.pressed = False
